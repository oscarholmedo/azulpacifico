<?php
$page = filter_input(INPUT_GET, 'page')? : 'inicio';
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Las Velas</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display|Lora' rel='stylesheet' type='text/css'>        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="css/mobile.css">
        <script type="text/javascript">
            if (jQuery(window).width() >= 700) {
                document.location = "index.php";
            }
        </script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    <body>
        <div class="header">
            <div class="container" style="">
                <div class="pull-right menu-icon"><img src="img/icon-menu.png"/></div>
                <div class="pull-left" style="padding-top: 10px;">
                    <a href="index.php"><img src="img/logo.png" alt="Las Velas"/></a>
                </div>
                <div class="collapse navbar" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li <?= (!$page || $page == 'inicio') ? 'class="active"' : '' ?>><a href="mobile.php?page=inicio">Inicio</a></li>
                        <li <?= ($page == 'concepto') ? 'class="active"' : '' ?>><a href="mobile.php?page=concepto">Concepto</a></li>
                        <li <?= ($page == 'modelos') ? 'class="active"' : '' ?>><a href="mobile.php?page=modelos">Modelos</a></li>
                        <li <?= ($page == 'amenidades') ? 'class="active"' : '' ?>><a href="mobile.php?page=amenidades">Amenidades</a></li>
                        <li <?= ($page == 'galeria') ? 'class="active"' : '' ?>><a href="mobile.php?page=galeria">Galeria</a></li>
                        <li <?= ($page == 'ubicacion') ? 'class="active"' : '' ?>><a href="mobile.php?page=ubicacion">Ubicacion</a></li>
                        <li <?= ($page == 'contacto') ? 'class="active"' : '' ?>><a href="mobile.php?page=contacto">Contacto</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <?php
        include "$page.php";
        ?>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.0.min.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b, o, i, l, e, r) {
                b.GoogleAnalyticsObject = l;
                b[l] || (b[l] =
                        function() {
                            (b[l].q = b[l].q || []).push(arguments)
                        });
                b[l].l = +new Date;
                e = o.createElement(i);
                r = o.getElementsByTagName(i)[0];
                e.src = '//www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', 'UA-XXXXX-X');
            ga('send', 'pageview');
        </script>
        <script>
            jQuery('.menu-icon').click(function(){
                jQuery('.navbar').removeClass('collapse');
            });
        </script>
    </body>
</html>
