<?php
if (filter_input(INPUT_POST, 'send')) {
    require 'PHPMailerAutoload.php';
    $mail = new PHPMailer();
    $mail->setFrom(filter_input(INPUT_POST, 'email'), filter_input(INPUT_POST, 'name'));
    $mail->addAddress('azulpacifico@mailinator.com', 'Azul Pacifico');
    $mail->Subject = filter_input(INPUT_POST, 'subject');
    $mensaje = ""
            . "<strong>Info request</strong>"
            . "<br/>"
            . "<br/>"
            . "De: " . filter_input(INPUT_POST, 'nombre')
            . "<br/>"
            . "Tel: "
            . filter_input(INPUT_POST, 'telefono')
            . "<br/>"
            . "Email: "
            . filter_input(INPUT_POST, 'email')
            . "<br/>"
            . "<br/>"
            . "Mensaje: "
            . "<br/>"
            . filter_input(INPUT_POST, 'mensaje')
    ;
    $mail->msgHTML($mensaje);
    $response = '';
    if (!$mail->send()) {
        $response = "Error de envio: " . $mail->ErrorInfo;
    } else {
        $response = "Mensaje enviado. Gracias.";
    }
    if ($response) {
        echo '<div class="alert alert-success alert-dismissible" " role="alert">';
        echo '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';
        echo $response;
        echo '</div>';
    }
}
?>
<div class="container" style="height:100%;">
	<br/>
    <h3>CONTACTO</h3>
    <div class="contact" style="width:300px;float:left;padding-right:40px;">
		<br/>
		<img src="img/lafher.png"/>
		<br/>
		<br/>
        <p>
		Con más de 10 años de experiencia en la construcción y promoción de conjuntos habitacionales de gran calidad y plusvalía en la ciudad de Culiacán, Sinaloa, LAFHER se ha distinguido por ofrecerle bienestar a todos sus clientes, con productos que trascienden la experiencia de compra, ofreciendo un valor percibido. 
		</p>
		<p>
		ventas@azulpacifico.mx
		<br/>
		info@lafher.com.mx
		</p>
		<p style="color:#f33;">01 800 002 2454</p>
		<p>azulpacifico.mx</p>
    </div>
    <div class="" style="width:620px;float:left;">
		<br/>
        <form action="" method="post">
            <label class="form-group">
                <span>Nombre</span>
                <input name="nombre" type="text" class="form-control input-sm"/>
            </label>
            <label class="form-group">
                <span>Email</span>
                <input name="email" type="text" class="form-control input-sm"/>
            </label>
            <label class="form-group">
                <span>Telefono</span>
                <input name="telefono" type="text" class="form-control input-sm"/>
            </label>
            <label class="form-group">
                <span>Asunto</span>
                <input name="subject" type="text" class="form-control input-sm"/>
            </label>
            <label class="form-group">
                <span>Mensaje</span>
                <textarea name="mensaje" class="form-control input-sm" style="width:610px;"></textarea>
            </label>
            <input name="send" value="1"  type="hidden"/>
			<br style="clear:both;"/>
            <button type="submit" class="btn btn-default btn-sm">Enviar</button>
        </form>
    </div>
    <div class="" style="float:left;">
		<img class="desktop" src="img/fish.jpg" style="margin:60px 20px;"/>
		<img class="mobile" src="img/fish.jpg" style="margin:20px 80px;"/>
		<br/>
		<br/>
	</div>
</div>

