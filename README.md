#Azul Pacifico #

* Feedback de revisiones, tareas, cambios, reporte de errores, etc. 
[Lista de Issues](https://bitbucket.org/oscarholmedo/azulpacifico/issues)

* [Crear un nuevo issue]([Crer un nuevo issue](https://bitbucket.org/oscarholmedo/azulpacifico/issues/new)) 


* Detalle de Archivos 
[Source](https://bitbucket.org/oscarholmedo/azulpacifico/src)


* Descarga de proyecto en Zip
 [Download](https://bitbucket.org/oscarholmedo/azulpacifico/get/6e74be138bf5.zip)

* Demo online [http://54.165.224.10/azulpacifico/](http://54.165.224.10/azulpacifico/)