<div class="container">
    <div class="gallery ">
		<div class="pic">
			<img src="img/photo-1.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-2.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-3.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-4.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-5.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-6.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-7.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-8.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-9.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-10.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-11.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="pic">
			<img src="img/photo-12.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
    </div>
</div>
<div class="footer">
	<div class="container">
		<div class="pull-left">
			<a href="#"><img src="img/twitter.png"/></a>
			<a href="#"><img src="img/facebook.png"/></a>
		</div>
		<div class="pull-right">
			<span>Informes 01 800 002 2425</span>
		</div>
	</div>
</div>
<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <button type="button" class="close" data-dismiss="modal" style="position:absolute;top:26px;right:30px;opacity:.6;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    function margins() {
        var gap;
		console.log(jQuery(window).width());
        if (jQuery(window).width() > 994) {
            gap = (jQuery('.gallery').width() - (220*4) ) / 3;
            jQuery('.pic').css({
				'margin-right':gap + 'px'
			});
            jQuery('.pic:nth-child(4n)').css({
				'margin-right':'0px'
			});
		}else{
            jQuery('.pic').css({
				'margin-right':'10px'
			});				
		}
    }
    jQuery(window).load(function() {
        margins();
    });
    jQuery(window).resize(function() {
        margins();
    });
    jQuery('.pic').click(function() {
        jQuery('.modal-body').html(jQuery(this).html());
        jQuery('.modal').modal();
    });
</script>