<div class="container ">
	<div class="card">
		<div class="pic">
			<img src="img/photo-1.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="red">LOFTS</div>
		<div class="blue">MEXICANO / CONTEMPORÁNEO <br/>65 m<sup>2</sup></div>
		<p>31 clusters de 3 pisos de altura y 9 unidades por edificio, con escaleras exteriores y fachadas de alta calidad, unidades diseñadas para las personas que les gusta vivir sin complicaciones en el mantenimiento de sus hogares.</p>
		<a href="pdf/lorem.pdf">VER PLANTAS</a>
	</div>
	<div class="card">
		<div class="pic">
			<img src="img/photo-1.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="red">LOFTS</div>
		<div class="blue">MEXICANO / CONTEMPORÁNEO <br/>65 m<sup>2</sup></div>
		<p>31 clusters de 3 pisos de altura y 9 unidades por edificio, con escaleras exteriores y fachadas de alta calidad, unidades diseñadas para las personas que les gusta vivir sin complicaciones en el mantenimiento de sus hogares.</p>
		<a href="pdf/lorem.pdf">VER PLANTAS</a>
	</div>
	<div class="card">
		<div class="pic">
			<img src="img/photo-1.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="red">LOFTS</div>
		<div class="blue">MEXICANO / CONTEMPORÁNEO <br/>65 m<sup>2</sup></div>
		<p>31 clusters de 3 pisos de altura y 9 unidades por edificio, con escaleras exteriores y fachadas de alta calidad, unidades diseñadas para las personas que les gusta vivir sin complicaciones en el mantenimiento de sus hogares.</p>
		<a href="pdf/lorem.pdf">VER PLANTAS</a>
	</div>
	<div class="card">
		<div class="pic">
			<img src="img/photo-1.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="red">LOFTS</div>
		<div class="blue">MEXICANO / CONTEMPORÁNEO <br/>65 m<sup>2</sup></div>
		<p>31 clusters de 3 pisos de altura y 9 unidades por edificio, con escaleras exteriores y fachadas de alta calidad, unidades diseñadas para las personas que les gusta vivir sin complicaciones en el mantenimiento de sus hogares.</p>
		<a href="pdf/lorem.pdf">VER PLANTAS</a>
	</div>
	<div class="card">
		<div class="pic">
			<img src="img/photo-1.jpg"/>
			<div class="shadow"></div>
			<div class="plus"></div>
		</div>
		<div class="red">LOFTS</div>
		<div class="blue">MEXICANO / CONTEMPORÁNEO <br/>65 m<sup>2</sup></div>
		<p>31 clusters de 3 pisos de altura y 9 unidades por edificio, con escaleras exteriores y fachadas de alta calidad</p>
		<a href="pdf/lorem.pdf">VER PLANTAS</a>
	</div>
</div>
<div class="footer">
	<div class="container">
		<div class="pull-left">
			<a href="#"><img src="img/twitter.png"/></a>
			<a href="#"><img src="img/facebook.png"/></a>
		</div>
		<div class="pull-right">
			<span>Informes 01 800 002 2425</span>
		</div>
	</div>
</div>
<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <button type="button" class="close" data-dismiss="modal" style="position:absolute;top:26px;right:30px;opacity:.6;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
    jQuery('.pic').click(function() {
        jQuery('.modal-body').html(jQuery(this).html());
        jQuery('.modal').modal();
    });
</script>
