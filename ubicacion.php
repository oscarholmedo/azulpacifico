<div  class="bkg-pull-right" style="background-image:none;">
    <div class="container" style="height:100%;">
        <div class="side-content">
            <h3><span style="color:#09c">EL COLOR</span><br/><span style="color:#f33;">— DE UN LUGAR PRIVILEGIADO</span></h3>
            <p>
                Azul Pacífico Residencias & Lofts, se encuentra ubicado al norte de la ciudad, dentro de la zona del Nuevo Mazatlán, en donde se localizan los nuevos centros comerciales, marina y hoteles de gran turismo de la ciudad.
            </p>
            <p>
                La distancia que se recorre desde el centro de la ciudad hacia Azul Pacífico, es de aproximadamente 15 min., ubicado en Zona Nuevo Mazatlán, #1700, Mazatlán, Sinaloa, entre el Hotel Riu y el Hotel Pueblo Bonito Emerald Bay.
            </p>
            <p>
                Además de tener esta excelente ubicación, en Azul Pacífico Residencias & Lofts, queremos que te diviertas sin tener que salir de casa.
            </p>
            <p style="width:300px;">
            <div class="pic">
                <img src="img/map1.jpg"/>
                <div class="shadow"></div>
                <div class="plus"></div>
            </div>
            <div class="pic" style="float:right;">
                <img src="img/map2.jpg"/>
                <div class="shadow"></div>
                <div class="plus"></div>
            </div>
            <br style="clear:both;"/>
            <br/>
            </p>
        </div>
    </div>
    <div class="side-map desktop" style="position:absolute;right:0;top:0;height:100%;width:100%;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3665.973730052554!2d-106.41237699999999!3d23.244042999999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869f530a0ecfc675%3A0x27d7fcd3523e0962!2sEl+Nino%2C+A.c.!5e0!3m2!1ses-419!2smx!4v1415300290299" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
    </div>
    <div class="side-map mobile" style="">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3665.973730052554!2d-106.41237699999999!3d23.244042999999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x869f530a0ecfc675%3A0x27d7fcd3523e0962!2sEl+Nino%2C+A.c.!5e0!3m2!1ses-419!2smx!4v1415300290299" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="pull-left">
            <a href="#"><img src="img/twitter.png"/></a>
            <a href="#"><img src="img/facebook.png"/></a>
        </div>
        <div class="pull-right">
            <span>Informes 01 800 002 2425</span>
        </div>
    </div>
</div>
<div class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            </div>
            <button type="button" class="close" data-dismiss="modal" style="position:absolute;top:26px;right:30px;opacity:.6;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
    .pic{
        width:90px;
        height:90px;
    }
    .pic img{
        width:100%;
        height:100%;
    }
</style>
<script>
    if (jQuery(window).width() > 699) {
        var mapwidth = jQuery(window).width() - jQuery('.side-content').offset().left - jQuery('.side-content').width() - 20;
        jQuery('.side-map').css({
            'width': mapwidth + 'px'
        });
    }
    jQuery('.pic').click(function () {
        jQuery('.modal-body').html(jQuery(this).html());
        jQuery('.modal').modal();
    });
</script>