<div class="fading">
    <div class="bkg " data-over="a" style="background-image: url(img/background-1.jpg);"></div>
    <div class="bkg 2" data-over="a" style="background-image: url(img/background-2.jpg);"></div>
    <div class="bkg 1" data-over="b" style="background-image: url(img/background-1.jpg);"></div>
</div>
<div class="container" style="position:relative;">
    <br/>
    <br/>
    <div class="home-titles" style="color:#fff;">
        <span style="font-size:36px;">MAZATLÁN</span> <br/><span>-EL NUEVO DESTINO <br/> TURÍSTICO DE MÉXICO</span>
    </div>
</div>
<div class="home-social">
    <a href="#"><img src="img/twitter.png"/></a>
    <a href="#"><img src="img/facebook.png"/></a>
</div>
<div class="informes">
    <span>Informes 01 800 002 2425</span>
</div>

<script>
    function fade(dom) {
        jQuery(dom).animate(
                {
                    'opacity': .0
                }, 1000,
                function() {

                }
        );
    }

    var itemBkg = 1;
    setInterval(
            function() {
                if (itemBkg === 1) {
                    jQuery('.bkg').css('opacity', '1');
                }
                fade('.bkg.' + itemBkg);
                itemBkg = itemBkg + 1;
                if (itemBkg === 4) {
                    itemBkg = 1;
                }
            }, 5000
            );
</script>