<div  class="bkg-pull-right" style="background-image:url(img/concepto.jpg);">
	<div class="container" >
		<div class="side-content">
			<h3><span style="color:#f33">CONCEPTO</span><br/><span style="color:#09c;">— AZUL PACÍFICO</span></h3>
			<p>
	Azul Pacífico, es un desarrollo residencial - horizontal, que cuenta con 149 residencias que van desde los 107m2 hasta los 200m2 y cuenta además con 186 departamentos tipo loft de 14mts. de frente x 19.5mts. de fondo c/u. El concepto arquitectónico con que se ha diseñado este proyecto, se basa en asignar diferentes áreas privadas dentro del proyecto, con una área común para cada una de ellas, lo que da como resultado el tener un desarrollo residencial mixto de alta calidad, tanto en las residencias y lofts como en las actividades exteriores que puedes realizar.
			</p>
			<p>
	La mezcla de dos estilos de arquitectura, contemporáneo y californiana, da como resultado un estilo único, que se adapta al entorno del terreno y al clima de la ciudad.
			</p>
			<p>
	El componente de seguridad y privacidad se da al tener una barda perimetral en todo el terreno que conforma el proyecto, con un solo acceso principal, que será vigilado y monitoreado las 24 horas del día. Todos los servicios son subterráneos, lo que nos permite tener un ambiente natural, libre de contaminación visual.
			</p>
			<p>VER MASTER PLAN
	— PLANTA DE CONJUNTO
			</p>
		</div>
	</div>
</div>
<div class="footer">
	<div class="container">
		<div class="pull-left">
			<a href="#"><img src="img/twitter.png"/></a>
			<a href="#"><img src="img/facebook.png"/></a>
		</div>
		<div class="pull-right">
			<span>Informes 01 800 002 2425</span>
		</div>
	</div>
</div>
<script>
    if (jQuery(window).width() > 699) {
		var bkg_offset = jQuery('.side-content').offset().left + jQuery('.side-content').width() + 21;
		jQuery('.bkg-pull-right').css({
			'background-position': bkg_offset+'px 0'
		});
	}
</script>