<div  class="bkg-pull-right" style="background-image:url(img/amenidades.jpg);">
    <div class="container">
        <div class="side-content">
            <h3><span style="color:#09c">AMENIDADES</span><br/><span style="color:#0c6;">— LIFESTYLE</span></h3>
            <p>
                Azul Pacífico, cuenta con un concepto de 4 áreas que ofrecen las mismas amenidades, en donde sólo una de ellas tiene diferente distribución sin alterar sus principales características, buscando la exclusividad y privacidad en cada una de ellas. Estas áreas contarán
                con alberca, área de chapoteadero con juegos infantiles acuáticos, camastros, casa club, camas de playa, asadores, baños para visitar y barra de usos múltiples, en donde podrás conocer los beneficios de vivir en este gran desarrollo residencial.
            </p>
        </div>
    </div>
</div>
<div class="footer">
    <div class="container">
        <div class="pull-left">
            <a href="#"><img src="img/twitter.png"/></a>
            <a href="#"><img src="img/facebook.png"/></a>
        </div>
        <div class="pull-right">
            <span>Informes 01 800 002 2425</span>
        </div>
    </div>
</div>
<script>
    if (jQuery(window).width() > 699) {
        var bkg_offset = jQuery('.side-content').offset().left + jQuery('.side-content').width()+ 21;
        jQuery('.bkg-pull-right').css({
            'background-position': bkg_offset + 'px 0'
        });
    }
</script>